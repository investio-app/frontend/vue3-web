# vue3-web

## Project setup

```
yarn install
```

Create a `.env.local` file

```
VUE_APP_API_URL=https://xxx.yyy
VUE_APP_WS_URL=wss://xxx.yyy
VUE_APP_MAINTAIN_MODE=no
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Run your end-to-end tests

```
yarn test:e2e
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
