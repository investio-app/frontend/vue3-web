export default [
  {
    fund_code: "SPOT33S7",
    cost: "3000",
    unit: "7.7242",
  },
  {
    fund_code: "SCBTG",
    cost: "1000",
    unit: "9.8493",
  },
  {
    fund_code: "Q-PORT",
    cost: "5000",
    unit: "9.2984",
  },
];
