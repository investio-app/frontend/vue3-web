import { httpClient } from "@/resources/httpClients";

const END_POINT = "public/v1";

const getTopPredict = () => httpClient.get(`${END_POINT}/predict`);

export { getTopPredict };
