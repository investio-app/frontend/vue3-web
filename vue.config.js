module.exports = {
  pwa: {
    workboxOptions: {
      exclude: [/_redirects/],
    },
  },
  devServer: {
    port: 2564,
    public: "https://2564-investioappfron-vue3web-tm6p8jai5st.ws.gp.dewkul.me",
    disableHostCheck: true,
    // allowedHosts: 'all'
  },
};
